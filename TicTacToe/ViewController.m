//
//  ViewController.m
//  TicTacToe
//
//  Created by Click Labs134 on 9/28/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"
NSArray *imageArray;
NSInteger *play;
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *ticTacToeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *ticTacToeBoard;
@property (strong, nonatomic) IBOutlet UILabel *turnLabel;
@property (strong, nonatomic) IBOutlet UILabel *turnMentioning;
@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIButton *button3;

@property (strong, nonatomic) IBOutlet UIButton *button4;
@property (strong, nonatomic) IBOutlet UIButton *button5;
@property (strong, nonatomic) IBOutlet UIButton *button6;
@property (strong, nonatomic) IBOutlet UIButton *button7;
@property (strong, nonatomic) IBOutlet UIButton *button8;
@property (strong, nonatomic) IBOutlet UIButton *button9;
@property (strong, nonatomic) IBOutlet UIButton *resetButton;

@end

@implementation ViewController
@synthesize ticTacToeBoard;
@synthesize ticTacToeLabel;
@synthesize turnLabel;
@synthesize turnMentioning;

- (void)viewDidLoad {
    [super viewDidLoad];
    imageArray=@[@"x.png",@"o.png"];
    play=1;
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)buttonReset:(UIButton *)sender {
    [self resetButton];

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
